<?php

namespace App\GraphqlController;

use App\Entity\User;
use App\Entity\UserToken;
use App\GraphqlException\InvalidCredentialsException;
use App\Service\UserToken\TokenProvider;
use App\Service\UserToken\UserTokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TheCodingMachine\GraphQLite\Annotations\Autowire;
use TheCodingMachine\GraphQLite\Annotations\InjectUser;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Mutation;

class AuthController extends AbstractGraphqlController
{
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        private UserPasswordHasherInterface $userPasswordHasher,
        private UserTokenGenerator $userTokenGenerator,
        private TokenProvider $tokenProvider
    )
    {
        parent::__construct($entityManager, $validator);
    }

    /**
     * @throws InvalidCredentialsException
     */
    #[Mutation]
    public function login(
        string $email,
        string $password
    ): UserToken
    {
        $userRepository = $this->getEntityManager()->getRepository(User::class);
        $user = $userRepository->findOneBy(['email' => $email]);
        if (!$user) {
            throw InvalidCredentialsException::create();
        }
        if (!$this->userPasswordHasher->isPasswordValid($user, $password)) {
            throw InvalidCredentialsException::create();
        }
        return $this->userTokenGenerator->generate($user);
    }

    #[Mutation, Logged]
    public function logout(#[InjectUser] ?User $user): bool
    {
        if (!$user) {
            return true;
        }
        $token = $this->tokenProvider->getToken();
        if (!$token) {
            return true;
        }
        $entityManager = $this->getEntityManager();
        $userTokenRepository = $entityManager->getRepository(UserToken::class);
        $userToken = $userTokenRepository->findOneBy(['token' => $token, 'user' => $user]);
        if ($userToken) {
            $entityManager->remove($userToken);
            $entityManager->flush();
        }
        return true;
    }
}