<?php

namespace App\GraphqlException;

use Symfony\Component\HttpFoundation\Response;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;

class InvalidCredentialsException extends GraphQLException
{
    public const CATEGORY_INVALID_CREDENTIALS = 'auth.invalid_credentials';

    public static function create(): self
    {
        return new self(
            'Invalid credentials',
            Response::HTTP_UNAUTHORIZED,
            category: self::CATEGORY_INVALID_CREDENTIALS
        );
    }
}