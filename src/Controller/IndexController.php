<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/', name: self::class, methods: ['GET'])]
class IndexController extends AbstractController
{
    public function __invoke(): JsonResponse
    {
        return $this->json(['ping' => (new \DateTime())->getTimestamp()]);
    }
}