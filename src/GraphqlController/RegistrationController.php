<?php

namespace App\GraphqlController;

use App\Entity\User;
use App\GraphqlException\UserAlreadyExistsException;
use App\GraphqlType\Registration;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TheCodingMachine\GraphQLite\Annotations\Autowire;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\Graphqlite\Validator\ValidationFailedException;

class RegistrationController extends AbstractGraphqlController
{
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        private UserPasswordHasherInterface $passwordHasher
    )
    {
        parent::__construct($entityManager, $validator);
    }

    /**
     * @throws UserAlreadyExistsException
     */
    #[Mutation]
    public function register(Registration $registration): bool
    {
        $errors = $this->getValidator()->validate($registration);
        ValidationFailedException::throwException($errors);

        $entityManager = $this->getEntityManager();
        $userRepository = $entityManager->getRepository(User::class);
        $exists = $userRepository->findOneBy(['email' => $registration->getEmail()]);
        if ($exists) {
            throw UserAlreadyExistsException::create();
        }
        $user = new User();
        $user
            ->setEmail($registration->getEmail())
            ->setPassword($this->passwordHasher->hashPassword($user, $registration->getPassword()));
        $entityManager->persist($user);
        $entityManager->flush();
        return true;
    }
}