<?php

namespace App\GraphqlController;

use App\Entity\User;
use App\GraphqlType\Builder\UserBuilder;
use App\GraphqlType\User as UserType;
use TheCodingMachine\GraphQLite\Annotations\InjectUser;
use TheCodingMachine\GraphQLite\Annotations\Query;

class UserController extends AbstractGraphqlController
{
    #[Query]
    public function getUser(#[InjectUser] ?User $user): ?UserType
    {
        return $user ? UserBuilder::buildFromEntity($user) : null;
    }
}