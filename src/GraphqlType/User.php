<?php

namespace App\GraphqlType;

use JetBrains\PhpStorm\ArrayShape;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
class User
{
    public function __construct(
        private string $email,
        private array $roles
    ) {}

    #[Field]
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string[]
     */
    #[Field]
    public function getRoles(): array
    {
        return $this->roles;
    }
}