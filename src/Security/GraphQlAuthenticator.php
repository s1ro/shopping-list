<?php

namespace App\Security;

use App\Repository\UserTokenRepository;
use App\Service\UserToken\TokenProvider;
use TheCodingMachine\GraphQLite\Security\AuthenticationServiceInterface;

class GraphQlAuthenticator implements AuthenticationServiceInterface
{
    public function __construct(
        private TokenProvider $tokenProvider,
        private UserTokenRepository $userTokenRepository
    ) {}

    public function isLogged(): bool
    {
        $user = $this->getUser();
        return isset($user);
    }

    public function getUser(): ?object
    {
        $token = $this->tokenProvider->getToken();
        if (!$token) {
            return null;
        }

        $userToken = $this->userTokenRepository->findOneBy(['token' => $token]);
        return $userToken?->getUser();
    }
}