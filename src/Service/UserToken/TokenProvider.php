<?php

namespace App\Service\UserToken;

use Symfony\Component\HttpFoundation\RequestStack;

class TokenProvider
{
    public function __construct(private RequestStack $requestStack) {}

    public function getToken(): ?string
    {
        return $this->requestStack->getCurrentRequest()->headers->get('X-AUTH-TOKEN');
    }
}