<?php

namespace App\Service\UserToken;

use App\Entity\User;
use App\Entity\UserToken;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class UserTokenGenerator
{
    public function __construct(private EntityManagerInterface $entityManager){}

    public function generate(User $user): UserToken
    {
        $token = new UserToken();
        $token
            ->setUser($user)
            ->setToken(Uuid::uuid4());
        $this->entityManager->persist($token);
        $this->entityManager->flush();
        return $token;
    }
}