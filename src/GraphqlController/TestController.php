<?php

namespace App\GraphqlController;

use TheCodingMachine\GraphQLite\Annotations\Query;

class TestController
{
    #[Query]
    public function test(string $message = 'test'): string
    {
        return $message;
    }
}