<?php

namespace App\GraphqlType\Factory;

use App\GraphqlType\Registration;
use TheCodingMachine\GraphQLite\Annotations\Factory;

class RegistrationFactory
{
    #[Factory]
    public function create(string $email, string $password): Registration
    {
        return new Registration($email, $password);
    }
}