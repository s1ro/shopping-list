<?php

namespace App\GraphqlType\Builder;

use App\Entity\User;
use App\GraphqlType\User as UserType;

class UserBuilder
{
    public static function buildFromEntity(User $user): UserType
    {
        return new UserType($user->getEmail(), $user->getRoles());
    }
}