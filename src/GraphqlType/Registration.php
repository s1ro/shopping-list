<?php

namespace App\GraphqlType;

use Symfony\Component\Validator\Constraints as Assert;

class Registration
{
    /**
     * @Assert\Email()
     */
    private string $email;

    /**
     * @Assert\Length(min="5")
     * @Assert\NotBlank()
     */
    private string $password;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}