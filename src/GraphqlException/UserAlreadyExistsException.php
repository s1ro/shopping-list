<?php

namespace App\GraphqlException;

use Symfony\Component\HttpFoundation\Response;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;

class UserAlreadyExistsException extends GraphQLException
{
    public const CATEGORY_USER_ALREADY_EXISTS = 'user.already_exists';

    public static function create(): self
    {
        return new self(
            'User already exists',
            Response::HTTP_BAD_REQUEST,
            category: self::CATEGORY_USER_ALREADY_EXISTS
        );
    }
}